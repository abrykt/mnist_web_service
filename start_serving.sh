#!/usr/bin/env bash

set -x
THIS_DIR=`pwd`

MODEL_PATH=$THIS_DIR/training/mnist

APP_NAME=tensorflow_model_server

command -v $APP_NAME >/dev/null 2>&1 || { PATH="${THIS_DIR}"/server/serving/bazel-bin/tensorflow_serving/model_servers/:$PATH; }
echo $PATH

$APP_NAME --port=9000 --model_name=mnist --model_base_path=$MODEL_PATH
