import requests
import unittest
from server.image_server import ImageServer
import base64
from server.mnist_recognizer import MnistRecognizer
from server.image_handler import make_handler
from test.mock_mnist_recognizer import MnistRecognizerMock
import time
from mock import Mock
import filecmp


class TestWebService(unittest.TestCase):

    def setUp(self):

        self.server_address = 'localhost'
        self.port = 8000

        self.mnist_recognizer_mock = Mock(spec=MnistRecognizer)
        self.mnist_recognizer_mock.recognize_letter = Mock(return_value = ([1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 'A'))
        handler = make_handler(self.mnist_recognizer_mock)

        self.server = ImageServer(address=self.server_address, port=self.port, handler=handler)

        self.server.start()
        time.sleep(1)

    def tearDown(self):
        time.sleep(1)
        self.server.shutdown()

    def test_post(self):
        url = 'http://{address}:{port}'.format(address=self.server_address, port=self.port)
        base64_data = base64.b64encode(b'bla bla')
        r = requests.post(url=url, data=base64_data)
        letter = r.content
        self.mnist_recognizer_mock.recognize_letter.assert_called()
        self.assertEqual(200, r.status_code)
        self.assertEqual(b'A', letter)


class TestWebServiceWithFakeImplementation(unittest.TestCase):

    def setUp(self):
        self.server_address = 'localhost'
        self.port = 8000
        self.test_image_path = 'test/a.png'
        self.decoded_path = "decoded.png"
        self.mnist_recognizer_mock = MnistRecognizerMock(self.decoded_path)
        handler = make_handler(self.mnist_recognizer_mock)
        self.server = ImageServer(address=self.server_address, port=self.port, handler=handler)
        self.server.start()
        time.sleep(1)

    def tearDown(self):
        time.sleep(1)
        self.server.shutdown()

    def test_posted_data(self):

        with open(self.test_image_path, 'rb') as f:
            url = 'http://{address}:{port}'.format(address=self.server_address, port=self.port)
            headers = {
                'Content-type': 'image/png',
                'Accept': '*/*',
            }

            data = f.read()
            bytes_base_64 = base64.b64encode(data)
            to_send = 'data:image/png;base64,{}'.format(bytes_base_64.decode('utf-8'))

            r = requests.post(url=url, headers=headers, data=to_send)

            is_equal = filecmp.cmp(self.test_image_path, self.decoded_path)
            self.assertTrue(is_equal)
            self.assertEqual(200, r.status_code)


if __name__ == '__main__':
    unittest.main()