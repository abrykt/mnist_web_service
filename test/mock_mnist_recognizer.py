import server.utils as utils
from server.mnist_recognizer import MnistRecognizer

class MnistRecognizerMock(MnistRecognizer):

    def __init__(self, decoded_path):
        self.decoded_path = decoded_path

    def recognize_letter(self, data):
        print('mock_recognize_letter called!!!!')
        self.data = data
        utils.write_decoded(self.data, self.decoded_path)
        return [1, 0, 0, 0, 0, 0, 0, 0, 0, 0], 'A'
