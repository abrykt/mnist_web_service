import numpy as np
import io
from PIL import Image
import unittest

from scipy import ndimage
import scipy
from server.mnist_recognizer import MnistRecognizer


class TestPreProcesse(unittest.TestCase):


    @unittest.skip
    def test_crop_box(self):
        a = np.array([[255, 255, 255, 255, 255],
                      [255, 255, 0,   0,   255],
                      [255, 255, 0,   0,   255],
                      [255, 255, 255, 255, 255],
                      [255, 255, 255, 255, 255]])
        x_low, x_high, y_low, y_high = MnistRecognizer.find_crop_box(a, low=255, high=0)
        crop = a[y_low:y_high + 1, x_low:x_high + 1]
        self.assertFalse(crop.any())

    @unittest.skip
    def test_crop_box_tilted(self):
        a = np.array([[255, 255, 0,   255, 255],
                      [255, 0,   0,   0,   255],
                      [255, 255, 0,   0,   255],
                      [255, 255, 255, 255, 255],
                      [255, 255, 255, 255, 255]])
        x_low, x_high, y_low, y_high = MnistRecognizer.find_crop_box(a, low=255, high=0)

        self.assertEqual(1, x_low)
        self.assertEqual(3, x_high)
        self.assertEqual(0, y_low)
        self.assertEqual(2, y_high)


    def test_real_image(self):
        image_file = 'test/test.png'

        img = ndimage.imread(image_file).astype(float)

        x_low, x_high, y_low, y_high = MnistRecognizer.find_crop_box(img, low=0, high=255)


        cropped = MnistRecognizer.crop(img=img, indices=(x_low, x_high, y_low, y_high))
        scipy.misc.imsave('cropped.png', cropped)

        scaled = MnistRecognizer.rescale(img=cropped)
        scipy.misc.imsave('cropped_scaled.png', scaled)

if __name__ == '__main__':
    unittest.main()