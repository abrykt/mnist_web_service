import unittest
import subprocess
import os
import signal
import time
from server.tensorflow_mnist_recognizer import TensorFlowMnistRecognizer
import numpy as np
from training.helpers import load_image, reformat, Parameters

class TestTfServing(unittest.TestCase):

    def setUp(self):
        print('starting tensorflow serving.')
        print(os.getcwd())
        cmd = ['./start_serving.sh']
        self.pro = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
        time.sleep(3)

        self.recognizer = TensorFlowMnistRecognizer(host='localhost', port='9000')

    def tearDown(self):
        print('Stopping tensorflow serving.')
        os.killpg(os.getpgid(self.pro.pid), signal.SIGTERM)
        time.sleep(2)


    def test_tf_serving_a(self):
        data = load_image('test/a.png')
        p = Parameters
        p.image_size = 28
        p.num_channels = 1
        data, _ = reformat(data, p)

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('A', letter)
        self.assertAlmostEqual(1, np.sum(scores))

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('A', letter)
        self.assertAlmostEqual(1, np.sum(scores))

        data = load_image('test/b.png')
        data, _ = reformat(data, p)

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('B', letter)
        self.assertAlmostEqual(1, np.sum(scores))

    def test_tf_serving_b(self):
        data = load_image('test/b.png')
        p = Parameters
        p.image_size = 28
        p.num_channels = 1
        data, _ = reformat(data, p)

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('B', letter)
        self.assertAlmostEqual(1, np.sum(scores))

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('B', letter)
        self.assertAlmostEqual(1, np.sum(scores))

        data = load_image('test/a.png')
        data, _ = reformat(data, p)

        scores, letter = self.recognizer.recognize_letter(data=data)
        self.assertEqual('A', letter)
        self.assertAlmostEqual(1, np.sum(scores))


if __name__ == '__main__':
    unittest.main()

