#!/usr/bin/env bash

set -x

THIS_DIR=`pwd`

sudo docker run -it --volume=${THIS_DIR}:/mnist_web_service         \
                    --workdir="/mnist_web_service"                  \
                    --memory=4g                         \
                     --memory-swap=4g                   \
                     --entrypoint=/bin/bash abrykt/tensorflow-serving:ver2
