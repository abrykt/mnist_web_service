# coding: utf-8

# Deep Learning
# =============
# 
# Assignment 4
# ------------
# 
# Previously in `2_fullyconnected.ipynb` and `3_regularization.ipynb`, we trained fully connected networks to classify
# [notMNIST](http://yaroslavvb.blogspot.com/2011/09/notmnist-dataset.html) characters.
# 
# The goal of this assignment is make the neural network convolutional.

# In[ ]:

# These are all the modules we'll be using later. Make sure you can import them
# before proceeding further.
from __future__ import print_function
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range
import numpy as np
from helpers import Parameters
import os
from tensorflow.python.saved_model import builder
from tensorflow.python.saved_model import signature_constants
from tensorflow.python.saved_model import signature_def_utils
from tensorflow.python.saved_model import utils
from tensorflow.python.saved_model import tag_constants
from helpers import maybe_download
from helpers import maybe_extract
from helpers import maybe_pickle
from helpers import merge_datasets
from helpers import randomize
from helpers import save_to_pickle
from helpers import reformat
from helpers import accuracy


train_filename = maybe_download('notMNIST_large.tar.gz', 247336696)
test_filename = maybe_download('notMNIST_small.tar.gz', 8458043)

np.random.seed(133)

train_folders = maybe_extract(train_filename)
test_folders = maybe_extract(test_filename)

train_datasets = maybe_pickle(train_folders, 45000)
test_datasets = maybe_pickle(test_folders, 1800)

train_size = 200000
valid_size = 10000
test_size = 10000

valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(
    train_datasets, train_size, valid_size)
_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)


train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)

data_root = '.'  # Change me to store data elsewhere

save_to_pickle(train_dataset, train_labels,
               valid_dataset, valid_labels,
               test_dataset, test_labels,
               data_root)

print('Training:', train_dataset.shape, train_labels.shape)
print('Validation:', valid_dataset.shape, valid_labels.shape)
print('Testing:', test_dataset.shape, test_labels.shape)

pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

# Reformat into a TensorFlow-friendly shape:
# - convolutions need the image data formatted as a cube (width by height by #channels)
# - labels as float 1-hot encodings.

# Parameters
p = Parameters()
p.image_size = 28
p.num_labels = 10
p.num_channels = 1  # grayscale
p.batch_size = 16
p.patch_size = 5
p.depth = 16
p.num_hidden = 64
p.num_steps = 10000

train_dataset, train_labels = reformat(train_dataset, p, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, p, valid_labels)
test_dataset, test_labels   = reformat(test_dataset, p, test_labels)

# Let's build a small network with two convolutional layers, followed by one fully connected layer. Convolutional
# networks are more expensive computationally, so we'll limit its depth and number of fully connected nodes.

graph = tf.Graph()

with graph.as_default():
    # Input data.
    tf_train_dataset = tf.placeholder(tf.float32, shape=(p.batch_size, p.image_size, p.image_size, p.num_channels))
    tf_serving_dataset = tf.placeholder(tf.float32, shape=(1, p.image_size, p.image_size, p.num_channels))

    tf_train_labels  = tf.placeholder(tf.float32, shape=(p.batch_size, p.num_labels))
    tf_valid_dataset = tf.constant(valid_dataset)
    tf_test_dataset  = tf.constant(test_dataset)

    # Variables.
    layer1_weights = tf.Variable(tf.truncated_normal([p.patch_size, p.patch_size, p.num_channels, p.depth], stddev=0.1))
    layer1_biases  = tf.Variable(tf.zeros([p.depth]))
    layer2_weights = tf.Variable(tf.truncated_normal([p.patch_size, p.patch_size, p.depth, p.depth], stddev=0.1))
    layer2_biases  = tf.Variable(tf.constant(1.0, shape=[p.depth]))
    layer3_weights = tf.Variable(tf.truncated_normal([p.image_size // 4 * p.image_size // 4 * p.depth, p.num_hidden], stddev=0.1))
    layer3_biases  = tf.Variable(tf.constant(1.0, shape=[p.num_hidden]))
    layer4_weights = tf.Variable(tf.truncated_normal([p.num_hidden, p.num_labels], stddev=0.1))
    layer4_biases  = tf.Variable(tf.constant(1.0, shape=[p.num_labels]))

    # Model.
    def model(data):
        conv    = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
        pool    = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        hidden  = tf.nn.relu(pool + layer1_biases)
        conv    = tf.nn.conv2d(hidden, layer2_weights, [1, 1, 1, 1], padding='SAME')
        pool    = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        hidden  = tf.nn.relu(pool + layer2_biases)
        shape   = hidden.get_shape().as_list()
        reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
        hidden  = tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases)

        return tf.matmul(hidden, layer4_weights) + layer4_biases


    # Training computation.
    logits = model(tf_train_dataset)
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_labels, logits=logits))

    # Optimizer.
    optimizer = tf.train.GradientDescentOptimizer(0.05).minimize(loss)

    # Predictions for the training, validation, and test data.
    train_prediction = tf.nn.softmax(logits)

    serving_prediction = tf.nn.softmax(model(tf_serving_dataset))
    valid_prediction = tf.nn.softmax(model(tf_valid_dataset))
    test_prediction  = tf.nn.softmax(model(tf_test_dataset))


with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    for step in range(p.num_steps):
        offset = (step * p.batch_size) % (train_labels.shape[0] - p.batch_size)
        batch_data   = train_dataset[offset:(offset + p.batch_size), :, :, :]
        batch_labels = train_labels[offset:(offset + p.batch_size), :]
        feed_dict = {
            tf_train_dataset: batch_data,
            tf_train_labels: batch_labels
        }
        _, l, predictions = session.run([optimizer, loss, train_prediction], feed_dict=feed_dict)
        if step % 50 == 0:
            print('Minibatch loss at step %d: %f' % (step, l))
            print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
            print('Validation accuracy: %.1f%%' % accuracy(valid_prediction.eval(), valid_labels))

    print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))

    # Export the model

    model_version = 1
    export_path_base = "mnist"

    export_path = os.path.join(export_path_base, str(model_version))

    print('Exporting trained model to', export_path_base)

    builder = builder.SavedModelBuilder(export_path)

    ## Build the signature_def_map.

    tensor_info_x = utils.build_tensor_info(tf_serving_dataset)
    tensor_info_y = utils.build_tensor_info(serving_prediction)

    prediction_signature = signature_def_utils.build_signature_def(
        inputs={
            'images': tensor_info_x
        },
        outputs={
            'scores': tensor_info_y
        },
        method_name=signature_constants.PREDICT_METHOD_NAME)

    classification_inputs = utils.build_tensor_info(tf_train_dataset)
    classification_outputs_classes = utils.build_tensor_info(tf_train_labels)
    classification_outputs_scores = utils.build_tensor_info(tf_train_labels)

    classification_signature = signature_def_utils.build_signature_def(
        inputs={
            signature_constants.CLASSIFY_INPUTS: classification_inputs
        },
        outputs={
            signature_constants.CLASSIFY_OUTPUT_CLASSES: classification_outputs_classes,
            signature_constants.CLASSIFY_OUTPUT_SCORES: classification_outputs_scores
        },
        method_name=signature_constants.CLASSIFY_METHOD_NAME)
    legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')

    signature_def_map = {
        'predict_images': prediction_signature,
        signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: classification_signature,
    }
    builder.add_meta_graph_and_variables(
        sess=session,
        tags=[tag_constants.SERVING],
        signature_def_map=signature_def_map,
        legacy_init_op=legacy_init_op)

    builder.save()

    print('Done exporting!')

p.Print()
