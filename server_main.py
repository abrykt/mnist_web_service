from argparse import ArgumentParser
from server.image_server import create_server

if __name__ == '__main__':
    usage = """
        usage: %prog
        """
    description = """
        """

    parser = ArgumentParser(usage=usage, description=description)
    parser.add_argument('-a', '--address', dest='address', default='localhost', help='Address of the server.')
    parser.add_argument('-p', '--port', type=int, dest='port', default=8000, help='port to listen to.')
    parser.add_argument('-ta', '--tf_address', dest='tf_address', default='localhost', help='Address of the tensorflow '
                                                                                            'serving instance.')
    parser.add_argument('-tp', '--tf_port', type=int, dest='tf_port', default=9000, help='port to where tensorflow '
                                                                                         'serving is listening.')

    args = parser.parse_args()
    server_address = args.address
    port = args.port
    tf_address = args.tf_address
    tf_port = args.tf_port


    server = create_server(server_address=server_address, port=port, tf_address=tf_address, tf_port=tf_port)

    try:
        server.start()
        input("Press enter to stop.\n")
    except Exception as e:
        server.shutdown()
        raise e


