from grpc.beta import implementations
import tensorflow as tf

from tensorflow_serving.apis import prediction_service_pb2
from tensorflow_serving.apis import predict_pb2
from google.protobuf.json_format import MessageToJson
import json
import numpy as np
from server.mnist_recognizer import MnistRecognizer
import io
from PIL import Image
from training.helpers import Parameters, reformat
import scipy


class TensorFlowMnistRecognizer(MnistRecognizer):

    def __init__(self, host, port):
        super(TensorFlowMnistRecognizer, self).__init__()

        self.channel = implementations.insecure_channel(host, int(port))
        self.stub = prediction_service_pb2.beta_create_PredictionService_stub(self.channel)

    def convert_to_format(self, data, pixel_depth=255):
        stream = io.BytesIO(data)
        img = Image.open(stream)
        img = img.convert('L')

        img = np.asarray(img)

        x_low, x_high, y_low, y_high = self.find_crop_box(img, low=0, high=255)
        indices = (x_low, x_high, y_low, y_high)
        print(indices)
        img = self.crop(img, indices=indices)
        img = self.rescale(img)
        scipy.misc.imsave('cropped_scaled.png', img)

        #Normalize
        img = (img.astype(float) - pixel_depth / 2) / pixel_depth

        p = Parameters
        p.image_size = 28
        p.num_channels = 1

        # Make vector with one dimension
        data, _ = reformat(img, p)
        return data

    def recognize_letter(self, data):
        request = predict_pb2.PredictRequest()
        request.model_spec.name = 'mnist'
        request.model_spec.signature_name = 'predict_images'

        request.inputs['images'].CopyFrom(tf.contrib.util.make_tensor_proto(data)) #, shape=[1, 28, 28, 1]))

        result = self.stub.Predict(request, 20)
        scores = self.get_scores(result)
        index = np.argmax(scores)
        letter = self.characters[index]
        return scores, letter


    def get_scores(self, proto_buf_response):
        json_string = MessageToJson(proto_buf_response)
        json_object = json.loads(json_string)
        results_list = json_object['outputs']['scores']['floatVal']

        return results_list



""""
tf.app.flags.DEFINE_string('server', 'localhost:9000',
                           'PredictionService host:port')
tf.app.flags.DEFINE_string('image', '', 'path to image in JPEG format')
FLAGS = tf.app.flags.FLAGS


def main(_):
    host, port = FLAGS.server.split(':')
    channel = implementations.insecure_channel(host, int(port))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)
    # Send request
    with open(FLAGS.image, 'rb') as f:
        # See prediction_service.proto for gRPC request/response details.
        data = f.read()
        request = predict_pb2.PredictRequest()
        request.model_spec.name = 'inception'
        request.model_spec.signature_name = 'predict_images'
        request.inputs['images'].CopyFrom(
            tf.contrib.util.make_tensor_proto(data, shape=[1]))
        result = stub.Predict(request, 10.0)  # 10 secs timeout
        print(result)


if __name__ == '__main__':
    tf.app.run()
"""
