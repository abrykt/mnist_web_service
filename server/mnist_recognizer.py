import numpy as np
import scipy
from PIL import Image

class MnistRecognizer(object):

    def __init__(self):
        self.characters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']

    def recognize_letter(self, image):
        """
        :param image:   numpy.ndarray containing the letter
        :return:        Tuple of (scores, letter) where scores are the probabilities of each of the characters and
                        letter is the most probable letter.
        """
        raise NotImplemented()


    def convert_to_format(self, data):
        return data

    @staticmethod
    def find_crop_box(a, low=0, high=255):
        index = np.argwhere(a == high)

        X = index[:, 1]

        x_low = np.min(X)
        x_high = np.max(X)

        Y = index[:, 0]

        y_low = np.min(Y)
        y_high = np.max(Y)

        return (x_low, x_high, y_low, y_high)

    @staticmethod
    def crop(img, indices):
        x_low = indices[0]
        x_high = indices[1]
        y_low = indices[2]
        y_high = indices[3]

        return img[y_low: y_high + 1, x_low:x_high + 1]

    @staticmethod
    def rescale(img, size=(28, 28)):
        img = Image.fromarray(np.uint8(img))
        scaled = img.resize(size=size)
        return np.array(scaled.getdata()).reshape((size[0], size[1]))


