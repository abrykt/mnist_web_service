import socketserver
from threading import Thread
from server.image_handler import make_handler
from server.tensorflow_mnist_recognizer import TensorFlowMnistRecognizer


def create_server(server_address, port, tf_address, tf_port):
    tensorflow_recognizer = TensorFlowMnistRecognizer(host=tf_address, port=tf_port)
    handler = make_handler(tensorflow_recognizer)
    server = ImageServer(address=server_address, port=port, handler=handler)
    return server


socketserver.TCPServer.allow_reuse_address = True


class ImageServer:

    def __init__(self, address, port, handler):
        self.address = address
        self.port = port
        self.httpd = socketserver.TCPServer((address, port), handler)

    def start(self):
        print("serving for {} on port {}".format(self.address, self.port))
        self.thread = Thread(target=self.httpd.serve_forever)
        self.thread.start()


    def shutdown(self):
        self.httpd.shutdown()
        self.httpd.socket.close()
        self.httpd.server_close()
        self.thread.join()
