import base64

def decode_png(post_data):
    start_index = post_data.find(b',')
    start_index = start_index if start_index > 0 else 0
    data = base64.b64decode(post_data[start_index:])
    return data

def write_decoded(data, path='decoded.png'):
    with open(path, 'wb') as f:
        f.write(data)


def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in
    this function because it is programmed to be pretty
    printed and may differ from the actual request.
    """
    print('{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))