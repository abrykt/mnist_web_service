from http.server import BaseHTTPRequestHandler
import server.utils as utils

def make_handler(recognizer):

    class ImageHandler(BaseHTTPRequestHandler, object):

        def __init__(self, request, client_address, server):
            self.recognizer = recognizer
            super(ImageHandler, self).__init__(request, client_address, server)
            self.protocol_version = 'HTTP/1.1'

        def log(self, message):
            print(message)
            print('\n')

        def log_message(self, format_, *args):
            self.log("Request: " + format_ % args)

        def do_POST(self):

            content_len = int(self.headers.get('Content-length', 0))
            self.log('Content-length {}'.format(content_len))
            post_body = self.rfile.read(content_len)

            data = utils.decode_png(post_body)

            utils.write_decoded(data)

            data = self.recognizer.convert_to_format(data)
            scores, letter = self.recognizer.recognize_letter(data)
            print('scores: {}'.format(scores))
            print('letter: {}'.format(letter))
            message = bytes(letter, 'utf-8')

            self.send_response(200)
            self.send_header('Access-Control-Allow-Origin', '*')
            self.send_header('Content-type', 'text/html')
            self.send_header("Content-Length", len(message))
            self.end_headers()
            self.wfile.write(message)

        def do_OPTIONS(self):
            self.send_response(200, 'ok')

            self.send_header('Access-Control-Allow-Origin', '*')
            self.send_header('Access-Control-Allow-Methods', 'POST, OPTIONS')
            self.send_header("Access-Control-Allow-Headers", "Content-Type")
            self.send_header("Content-Length", 0)
            self.end_headers()


        def log_info(self):
            print("self.headers.dict['origin'] " + self.headers.dict['origin'])

    return ImageHandler