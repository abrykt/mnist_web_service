# mnist web server

This project contains a simple web page that lets you draw an image of a letter. The page sends the image
to a server which sends the image to a tensorflow-serving instance serving a model trained to recognize the letter. The
recognized letter is the sent back to the webpage and displayed to the user.

Try it out at [http://mnist.brykt.se/](http://mnist.brykt.se/). Response time is a bit slow the first time you send a query, 
this due to the server running in a virtual machine with limited resources.

## Install
Checkout this repository. Checkout all submodules with ```git submodule update --init --recursive```.

### Folder structure
```
server_main.py
    Use this to start your server.

start_serving.sh
    Use this to start tensorflow serving.
    
+-- frontend
|   Simple webpage that displays a canvas for the user to draw a letter on and send it to the server for recognition.
|
+-- server
|   This folder includes the server, http handler, a wrapper around the classes used to communicate with the tensorflow
|   serving instance.
|
|   +-- tensorflow_serving
|       Checked in protbuf stubs, which can also be generated from tensorflow-serving.
| 
+-- test
|   Unittest
|
+-- training
|   Script to train the tensorflow model including utility functions to download images.
```

### python
I used python 3 and ubuntu 16.04 when developing this. Dependencies can be installed
with `pip3 install -r requirements.txt`

### Tensorflow
Tensorflow is on Windows only installable with pip with python 3.5.2 You can build Tensorflow from source for any
python version. On Linux and MacOs it should be installable with pip for any python version.

### Tensorflow-serving
Instructions can be found here [https://tensorflow.github.io/serving/setup](https://tensorflow.github.io/serving/setup)

## Running

### Training the tensorflow model
There is a tensorflow model checked in to this repository in `training/mnist_model/0`. Feel free to use it. If you want
 to train your own model you can modify `training.py`. Run it with:

`cd training`

`python training.py`

This will create a new model in `training/mnist_model/1`. Modify `model_version` in `training.py` to create additional
versions.

The first time you run this script the training data will be downloaded and preprocessed. The preprocessed data is then
saved for later use. The script will go on and train a model and save it under `training/mnist_model`.

### Running the tensorflow-serving
Run `start_serving.sh` to start serving the tensorflow model. Please check the script how to pass address and port to
the request handler and how to set address and port to the tensorflow-serving instance that is serving your model.

## Testing
To run unittests: `python3 -m unittest discover`.

Status of the latest build can be found at [here](https://bitbucket.org/abrykt/mnist_web_service/addon/pipelines/home#!/).

### Contact
Feel free to contact me at abrykt@gmail.com.




