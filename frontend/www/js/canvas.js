var canvas = false;
var ctx = false;
var flag = false;
var prevX = 0;
var currX = 0;
var prevY = 0;
var currY = 0;
var dot_flag = false;
var penColor = "white";
var penWidth = 20;

function init() {
    canvas = document.getElementById('can');
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    canvas.addEventListener("mousemove", function (e) {
        findxy('move', e)
    }, false);
    canvas.addEventListener("mousedown", function (e) {
        findxy('down', e)
    }, false);
    canvas.addEventListener("mouseup", function (e) {
        findxy('up', e)
    }, false);
    canvas.addEventListener("mouseout", function (e) {
        findxy('out', e)
    }, false);
}

function draw()
{
    ctx.beginPath();
    ctx.moveTo(prevX, prevY);
    ctx.lineTo(currX, currY);
    ctx.strokeStyle = penColor;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.lineWidth = penWidth;
    ctx.stroke();
    ctx.closePath();
}

function findxy(res, e)
{
    if (res == 'down')
    {
        prevX = currX;
        prevY = currY;
        currX = e.clientX - canvas.offsetLeft;
        currY = e.clientY - canvas.offsetTop;

        flag = true;
        dot_flag = true;
        if (dot_flag)
        {
            ctx.beginPath();
            ctx.fillStyle = penColor;
            ctx.fillRect(currX, currY, 5, 5);
            ctx.closePath();
            dot_flag = false;
        }
    }

    if (res == 'up' || res == "out")
    {
        flag = false;
    }
    if (res == 'move')
    {
        if (flag)
        {
            prevX = currX;
            prevY = currY;
            currX = e.clientX - canvas.offsetLeft;
            currY = e.clientY - canvas.offsetTop;
            draw();
        }
    }
}

function onClearClicked()
{
    erase()
}

function erase()
{
    ctx.clearRect(0, 0, w, h);
    canvas = document.getElementById('can');
    ctx = canvas.getContext("2d");
    w = canvas.width;
    h = canvas.height;

    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    document.getElementById('textbox').value = '';
}

function onSendClicked()
{
    var dataURL = canvas.toDataURL();
    var theUrl = 'http://ml.brykt.se';
    send(dataURL, theUrl, parseAndShowResult)
}

function parseAndShowResult(result)
{
    //window.alert(result);
    //#console.log(result);
    document.getElementById('textbox').value = result
}

function send(dataUrl, serviceUrl, onAnswer)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function ()
    {
        if(xmlHttp.readyState === 4)
        {
            if(xmlHttp.status === 200)
            {
                onAnswer(xmlHttp.responseText)
            }
            else
            {
                onAnswer("An error occurred.")
            }
        }

    };
    xmlHttp.open("POST", serviceUrl, true);
    xmlHttp.setRequestHeader('Content-type', 'image/png');
    xmlHttp.send(dataUrl);
}
